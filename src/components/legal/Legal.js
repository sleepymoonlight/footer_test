import React from "react";
import PropTypes from "prop-types";

import "./styles.scss";

const Legal = ({ desktopText = '', mobileText = [] }) => {
  return (
    <div className="footer-legal">
      <div className="desktop">{desktopText}</div>
      <div className="mobile">
        {mobileText.length && mobileText.map((data) => (
          <p key={data}>{data}</p>
        ))}
      </div>
    </div>
  );
};

Legal.propTypes = {
    mobileText: PropTypes.array,
    desktopText: PropTypes.string,
}

export default Legal;
