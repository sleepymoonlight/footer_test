import React from "react";
import PropTypes from "prop-types";
import Accordion from "@material-ui/core/Accordion";
import AccordionSummary from "@material-ui/core/AccordionSummary";
import AccordionDetails from "@material-ui/core/AccordionDetails";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";

import "./styles.scss";

const Navigation = ({mobileData = [], desktopData= []}) => {
    const renderMobileNav = () => {
        return mobileData.length ? mobileData.map((data) => (
            <div className="navigation-item mobile-item" key={data._id}>
                {data.navLinks && data.navLinks.length ? (
                    <Accordion classes={{root: "accordion-wrapper", expanded: "expanded"}}>
                        <AccordionSummary
                            expandIcon={<ExpandMoreIcon/>}
                            aria-controls="panel1a-content"
                            id="panel1a-header"
                            classes={{
                                root: "accordion-summary",
                                expanded: "expanded",
                                expandIcon: "expandIcon"
                            }}
                        >
                            <h5>{data.displayText}</h5>
                        </AccordionSummary>
                        <AccordionDetails classes={{root: "accordion-details"}}>
                            {data.navLinks.map((item) => (
                                <a href={item.url} key={item._id}>
                                    {item.displayText}
                                </a>
                            ))}
                        </AccordionDetails>
                    </Accordion>
                ) : (
                    <a href={data.url}>{data.displayText}</a>
                )}
            </div>
        )) : null;
    };

    const renderDesktopNav = () => {
        return desktopData.length ? desktopData.map((data) => (
            <div className="navigation-item" key={data._id}>
                <h5>{data.displayText}</h5>
                {data.navLinks.map((item) => (
                    <a href={item.url} key={item._id}>
                        {item.displayText}
                    </a>
                ))}
            </div>
        )) : null;
    };

    return (
        <>
            <div className="navigation-container desktop">{desktopData && renderDesktopNav()}</div>
            <div className="navigation-container mobile">{mobileData && renderMobileNav()}</div>
        </>
    );
};

Navigation.propTypes = {
    mobileData: PropTypes.array,
    desktopData: PropTypes.array,
}

export default Navigation;
