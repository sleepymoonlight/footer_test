import React from "react";
import PropTypes from "prop-types";

import Navigation from "./../../components/navigation/Navigation";
import Legal from "../../components/legal/Legal";
import logo from "../../assets/images/logo_white.svg";
import insta from "../../assets/images/insta.svg";
import fb from "../../assets/images/fb.svg";
import badge from "../../assets/images/badge.svg";
import twitter from "../../assets/images/twitter.svg";

import "./styles.scss";

const dataPropTypes = {
    legalText: PropTypes.array,
    name: PropTypes.string,
    navLinkGroups: PropTypes.array,
    base_defense: PropTypes.number,
}

const Footer = ({desktopFooter, mobileFooter}) => {
    const desktopLegalText = desktopFooter?.legalText?.length && desktopFooter.legalText[0].children.length && desktopFooter.legalText[0].children[0].text || '';

    const mobileLegalText = () => {
        return mobileFooter && mobileFooter.legalText?.map(data => {
            return data?.children.length && data.children[0].text || '';
        })
    };
    return (
        <footer className="footer">
            <div className="footer-wrapper">
                <div className="footer-data-container">
                    <div className="footer-logo-container">
                        <img src={logo} alt="logo" className="logo-icon"/>
                        <div className="footer-social">
                            <img src={insta} alt="instagram"/>
                            <img src={fb} alt="fb"/>
                            <img src={badge} alt="badge"/>
                            <img src={twitter} alt="twitter"/>
                        </div>
                    </div>
                    <Navigation
                        mobileData={mobileFooter && mobileFooter.navLinkGroups}
                        desktopData={desktopFooter && desktopFooter.navLinkGroups}
                    />
                </div>
            </div>
            <Legal
                desktopText={desktopLegalText}
                mobileText={mobileLegalText()}
            />
        </footer>
    );
};

Footer.propTypes = {
    desktopFooter: PropTypes.shape(dataPropTypes),
    mobileFooter: PropTypes.shape(dataPropTypes),
}

export default Footer;
