import Footer from "./footer/Footer";
import { desktopFooter, mobileFooter } from "../mockData";

export default () => {
  return <Footer desktopFooter={desktopFooter} mobileFooter={mobileFooter} />;
};
